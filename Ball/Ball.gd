extends KinematicBody2D

var speed = 600
var direction = Vector2.ZERO

func _ready():
	randomize()
	direction.x = [-1, 1][randi() % 2]
	direction.y = [-1, -1][randi() % 2]
	
func _physics_process(delta):
	var collision_object = move_and_collide(direction * speed * delta)
	if collision_object:
		direction = direction.bounce(collision_object.normal)
