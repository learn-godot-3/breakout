extends KinematicBody2D

var speed = 400

func _physics_process(_delta):
	var direction = Vector2.ZERO
	if Input.is_action_pressed('ui_left'):
		direction.x = -1
	if Input.is_action_pressed(('ui_right')):
		direction.x = 1
	if not (Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		direction.x = 0
		
	var _collision_object = move_and_slide(direction * speed)
